// Dependencies
const
	modem		= require('modem').Modem(),
	firebase	= require('firebase-admin'),
	mongoose	= require('mongoose')
;

// Server Configuration
const config	= require('./config');

// Libraries
const util		= require('./libs/utilities');
const modemEmulator = require('./libs/modem-emulator');

// Dependency Initializations
let initializeDependencies = new Promise((resolve, reject) => {

	// Firebase
	firebase.initializeApp({
		credential: firebase.credential.cert(require('./credentials/puy9e-f1c205e494')),
		databaseURL: 'https://techexperts-sms-gateway.firebaseio.com'
	});

	util.log('Firebase', 'Connection has been established.', 'important');

	// MongoDB via Mongoose
	mongoose.Promise = global.Promise;

	if(!mongoose.connection._readyState) {

		mongoose.connect(config.mongodb.host, config.mongodb.database, config.mongodb.port).then(
			() => util.log('MongoDB', 'Connection authenticated.', 'important'),
			() => {
				util.log('MongoDB', `Cannot connect to ${config.mongodb.host}:${config.mongodb.port}`, 'error');
				reject();
			}
		);

		let connection = mongoose.connection;

		connection.on('open', () => {

			// Database Schemas
			global.Backup = require('./schemas/backup');

			util.log('MongoDB', 'Connection has been established.', 'important');
			resolve();
		});

		connection.on('error', () => {
			util.log('MongoDB', 'Error establishing connection.', 'error');
			reject();
		});
	} else reject();

});

// Main Algorithm
initializeDependencies.then(() => {

	const
		ports	= config.gsmPorts,
		clients	= config.clients
	;

	let portIndex = 0;

	// Listen for new data from Clients
	clients.forEach(client => {

		// Firebase database instance
		let database = firebase.database().ref(client)
			.orderByChild('status')						// In SQL, this is similar to
			.equalTo('sending')							// `SELECT * FROM client WHERE status = 'sending'
		;

		util.log('firebase/'+client, 'Listening for new data...', 'important');

		// Everytime a new data is received
		database.on('child_added', data => {

			util.log('firebase/'+client,'New data received.');

			// Reset to first port if already using the last one
			if(portIndex === ports.length) portIndex = 0;

			let backupData = data.val();
			
			backupData.firebaseKey = data.key;

			// [Temporary] Most data on test Database doesn't have contact numbers.
			backupData.contactNo = backupData.contactNo || '8080';
			
			let backup = new Backup(backupData);

			// Assign next port
			backup.port = ports[portIndex++];

			backup.save(err => {
				if(err) throw err;
				util.log('MongoDB', 'Data from "firebase/' + client + '" has been cached. Message assigned to port ' + backup.port);
			});
		});

	});

	// Open all registered ports
	ports.forEach(port => {

		/* NodeJS module `Modem` doesnt have a fallback
		 * when it fails connecting to a port. It will
		 * explicitly throw an error.
		 *
		 * Ports should be verified connected and correct
		 * inside `config.json` file.
		 */
		modemEmulator.open(port, () => {
			util.log(port, 'Port connection opened.');

			let cacheCheckTimeout = config.cacheCheck.minTimeout;	// Default waiting time in seconds before checking cache again
			let cacheCheckStatus = 'sending';						// Status to check. `sending` by default. Then `failed` on first round of rechecking
			let checkCache = () => {

				// Check pending message for this port
				Backup.findOne({port: port, status: cacheCheckStatus}, (err, data) => {
					if(err) throw err;

					if(data) {

						// If a message is returned
						let sendMessage = new Promise((resolve, reject) => {

							// Construct SMS Data
							let message = {
								text: `FROM ${data.schoolId}'\n'ID#(${data.studentId}) - TIME ${data.checkType.toUpperCase()} - ${new Date(data.checkTime).toLocaleString()}`,
								receiver: data.contactNo,
								encoding: '16bit'
							};

							// Send message
							modemEmulator.sms(message, (err, ref) => {

								if(err) {
									// If message sending failed
									util.log(`GSM@${port}`, `Message to ${data.contactNo}@${data.schoolId} sending failed.`);

									// Set status to failed
									data.status = 'failed';
									data.save(err => {
										if(err) throw err;
										util.log(`GSM@${port}`, `Message to ${data.contactNo}@${data.schoolId} has been requeued.`);
									});

									resolve();
								} else {
									// If message was sent
									util.log(`GSM@${port}`, `Message to ${data.contactNo}@${data.schoolId} sent.`);

									// Set status to sent
									data.status = 'sent';
									data.save(err => {
										if(err) throw err;
										util.log(`GSM@${port}`, `Message to ${data.contactNo}@${data.schoolId} has been declared sent.`);

										// Update firebase copy of data to {status: 'sent'}
										firebase.database().ref([data.schoolId, data.firebaseKey, 'status'].join('/')).set('sent');

										// Delete sent message from SIM storage
										ref.forEach(smsIndex => {
											modemEmulator.deleteMessage(smsIndex, () => {
												/* [BUG] Most of the time, NodeJS module `Modem` doesnt
												 * execute this callback. But the message still gets deleted.
												 */
												util.log(`GSM@${port}`, `Message with Index ID ${smsIndex} has been deleted.`);
											});
										});
									});

									resolve();
								}
							});

						});

						// After processing a message
						sendMessage.then(() => {
							cacheCheckTimeout = 5;	// Reset cacheCheckTimeout delay
							checkCache();			// Check cache again
						});

					} else {
						// Else, if no message was returned
						util.log(`GSM@${port}`, `This port has no more pending messages. Checking again after ${cacheCheckTimeout} seconds.`);

						/* If its the first round to re-check for messages,
						 * check messages with status 'failed' first and
						 * resend them all.
						 */
						cacheCheckStatus = (cacheCheckStatus == 'sending' && cacheCheckTimeout == 5) ? 'failed' : 'sending';

						// Wait for defined checkCacheTimeout time before checking again...
						setTimeout(() => {
							util.log(`GSM@${port}`, `Now checking for ${cacheCheckStatus} messages.`);
							if(cacheCheckTimeout < config.cacheCheck.maxTimeout) cacheCheckTimeout += config.cacheCheck.minTimeout; // Add 5 more seconds to the delay...
							checkCache();			// Check cache again
						}, cacheCheckTimeout * 1000);

					}

				});

			};

			checkCache();

		});

	});

}, () => util.log('SERVER', 'Fatal error. Execution halted.', 'error'));