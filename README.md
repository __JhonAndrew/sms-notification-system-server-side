# TechExpertsPH - SMS Notification System
#### Server-side Server

> [Jhon Andrew Q. Baes](https://fb.me/mongsangga) | <anecbook@gmail.com> / [Messenger](https://m.me/mongsangga)

* * *

##### Setup
1. First, install the dependencies.
	* `npm install`
2. Test the server.
	* `npm test`

* * *

#### System Flow

##### A. Receiving Messages from Clients@Firebase

1. Loop to each School ID (`config.clients`) and register a new data event listener.
2. When a data is received, create a backup (cache).
3. Assign current port to backup. Then prepare next port. (`port++`).
4. Save (cache) backup to MongoDB.

##### B. Sending Cached Messages

1. Loop to each registered ports (`config.gsmPorts`) and register a cron job.
2. Check one cache (backup) with `{status: 'sending'}`
3. If a result is returned, construct and send a message.
4. If message successfully sent, set `{status: 'sent'}`. Else, set `{status: 'failed'}`.
5. Repeat step 2. If there are no more `{status: 'sending'}` messages. Check for `{status: 'failed'}`
6. If there are `{status: 'failed'}` messages returned, go back to step 3.
7. Else, go back to step 6.

* * *

##### Notes

* On line `128` and `154`, change `modemEmulator` to `modem` to test on actual GSM Modems.
* The algorithm will not send `{status: 'sending'}` messages until there are already no `{status: 'failed'}` messages. A feature or a bug? _haha! Will be fixing this._
* Open `config.json` and add more values to `gsmPorts` array to add more GSM Modems.