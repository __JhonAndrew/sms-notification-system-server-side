const mongoose	= require('mongoose');

let BackupSchema = new mongoose.Schema({
	firebaseKey: String,
	contactNo: String,
	schoolId: String,
	status: String,
	studentId: String,
	studentName: String,
	checkTime: Date,
	checkType: String,
	port: String
});

BackupSchema.methods.messageSent = () => {
	this.status = 'sent';
	return this.status;
};

module.exports = mongoose.model('Backup', BackupSchema);