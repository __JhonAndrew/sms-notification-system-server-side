const util = require('./utilities');
module.exports = {
	open(port, callback) {
		util.log(`GSM@${port}`, 'Emulated port opened.', 'important');
		callback();
	},
	sms(message, callback) {
		let sendStatus = (util.rand(0, 1)) ? true : false;

		setTimeout(() => {
			if(sendStatus) callback(true, null);		// Emulate send
			else callback(null, [util.rand(1,100)]);	// Emulate failed
		}, util.rand(1, 5) * 1000);						// Emulate sending delay
	},
	deleteMessage(msgIndex, callback) {
		callback();
	}
}